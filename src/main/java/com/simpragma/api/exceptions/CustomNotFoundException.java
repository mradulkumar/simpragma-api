package com.simpragma.api.exceptions;

public class CustomNotFoundException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int errorCode;

	public CustomNotFoundException(String message) {
		super(message);
	}

	public CustomNotFoundException(int errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}

	public int getErrorCode() {
		return errorCode;
	}
}