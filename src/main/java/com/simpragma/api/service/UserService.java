package com.simpragma.api.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;

import com.simpragma.api.exceptions.CustomNotFoundException;
import com.simpragma.api.model.User;
import com.simpragma.api.repository.UserRepository;
import com.simpragma.api.util.Contants;

@Controller
public class UserService implements UserDetailsService {
	private static Log logger = LogFactory.getLog(UserService.class);

	@Autowired
	private UserRepository usersRepository;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	public String saveUser(User user) throws CustomNotFoundException {
		logger.info("saving user.....");
		Optional<User> isexist = usersRepository.findByEmail(user.getEmail());
		if (isexist.isPresent())
			throw new CustomNotFoundException(Contants.ALREADY_EXITS_USER_CODE, Contants.ALREADY_EXITS_USER);

		user.setCreateDate(new Timestamp(new Date().getTime()));
		user.setPassword(bcryptEncoder.encode(user.getPassword()));
		usersRepository.save(user);
		return "{\"message\": \"new user created....\"}";
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<User> user = usersRepository.findByUsername(username);
		if (!user.isPresent()) {
			throw new UsernameNotFoundException(Contants.INVALID_USER + username);
		}
		return new org.springframework.security.core.userdetails.User(user.get().getUsername(),
				user.get().getPassword(), new ArrayList<>());
	}

}
