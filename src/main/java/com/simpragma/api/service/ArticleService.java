package com.simpragma.api.service;

import java.sql.Timestamp;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;

import com.simpragma.api.exceptions.CustomNotFoundException;
import com.simpragma.api.model.Article;
import com.simpragma.api.repository.ArticleRepository;

@Controller
public class ArticleService {
	private static Log logger = LogFactory.getLog(ArticleService.class);

	@Autowired
	private ArticleRepository articleRepository;

	public String saveArticle(Article article) throws CustomNotFoundException {
		logger.info("saving article.....");
		article.setCreateDate(new Timestamp(new Date().getTime()));
		articleRepository.save(article);
		return "{\"message\": \"new article created....\"}";
	}

	public Page<Article> getAllArticle(Pageable pageable) throws CustomNotFoundException {
		logger.info("getAll article.....");
		return articleRepository.findAll(pageable);
	}
}
