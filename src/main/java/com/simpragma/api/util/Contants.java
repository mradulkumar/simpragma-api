package com.simpragma.api.util;

public final class Contants {

	public final static int ALREADY_EXITS_USER_CODE = 1000;
	public static String ALREADY_EXITS_USER = "User already exists";
	public final static int INVALID_USER_CODE = 1001;
	public static String INVALID_USER = "User not found with username: ";

	public final static int INCORRECT_CRED_CODE = 1001;
	public static String INCORRECT_CRED = "Password incorrect ";

}
