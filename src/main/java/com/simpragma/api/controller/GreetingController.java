package com.simpragma.api.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.simpragma.api.exceptions.CustomError;
import com.simpragma.api.exceptions.CustomNotFoundException;
import com.simpragma.api.model.User;
import com.simpragma.api.service.UserService;

@RestController
@CrossOrigin()
public class GreetingController {
	private static Log logger = LogFactory.getLog(GreetingController.class);

	@RequestMapping(value = "/greeting", method = RequestMethod.GET)
	public String home() {
		return "Hello Simpragma...!!";
	}
}
