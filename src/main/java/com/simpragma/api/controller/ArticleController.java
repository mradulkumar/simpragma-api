package com.simpragma.api.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.simpragma.api.exceptions.CustomError;
import com.simpragma.api.exceptions.CustomNotFoundException;
import com.simpragma.api.model.Article;
import com.simpragma.api.model.User;
import com.simpragma.api.service.ArticleService;

@RestController
@RequestMapping("/article")
public class ArticleController {
	private static Log logger = LogFactory.getLog(ArticleController.class);

	@Autowired
	private ArticleService articleController;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> saveArtilce(@RequestBody Article article) {
		try {
			return new ResponseEntity<String>(articleController.saveArticle(article), HttpStatus.CREATED);
		} catch (CustomNotFoundException e) {
			CustomError errorResponse = new CustomError(HttpStatus.BAD_REQUEST, e.getErrorCode(), e.getMessage());
			return new ResponseEntity<CustomError>(errorResponse, errorResponse.getStatus());
		}
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> getAllArticle(Pageable pageable) {
		try {
			return new ResponseEntity<Page<Article>>(articleController.getAllArticle(pageable), HttpStatus.OK);
		} catch (CustomNotFoundException e) {
			CustomError errorResponse = new CustomError(HttpStatus.BAD_REQUEST, e.getErrorCode(), e.getMessage());
			return new ResponseEntity<CustomError>(errorResponse, errorResponse.getStatus());
		}
	}
}
