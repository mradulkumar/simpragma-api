package com.simpragma.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.simpragma.api.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	public Optional<User> findByEmail(String email);

	public Optional<User> findByUsername(String username);

	public Optional<User> findByEmailAndPassword(String email, String password);

}
