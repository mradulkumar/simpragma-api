package com.simpragma.api.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.simpragma.api.model.Article;

public interface ArticleRepository extends PagingAndSortingRepository<Article, Integer> {

	Page<Article> findAll(Pageable pageable);

//	Page<Announcement> findByRentalsitesIdInOrderByCreateDateDesc(List<Integer> rentalSiteIds, Pageable pageable);

}
